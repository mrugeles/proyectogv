{

"header" : {
  "navbar" : {
    "UPLOAD" : "Upload",
    "new" : {
      "NEW" : "New",
      "PROJECT" : "Projects",
      "TASK" : "Task",
      "USER" : "User",
      "EMAIL" : "Email"
    },
    "NOTIFICATIONS" : "Notifications"
  }
},
"aside" : {
  "nav" : {
    "HEADER" : "Navigation",
    "DAYLY" : "Labores del día",
    "FUNNEL" : "Funnel",
    "CLIENTS" : "Clientes",
    "CALENDAR" : "Calendario",
    "GOALS" : "Metas",
    "NOTIFICATIONS" : "Notificaciones",
    "your_stuff" : {
      "YOUR_STUFF": "Cuenta",
      "PROFILE" : "Perfil",
      "DOCUMENTS" : "Documentos"
    }
  },
  "GOALS" : "Metas",
  "RELEASE" : "Release"
}

}
