
/** google global namespace for Google projects. */
var google = google || {};

/** appengine namespace for Google Developer Relations projects. */
google.appengine = google.appengine || {};

/** samples namespace for App Engine sample code. */
google.appengine.gv = google.appengine.gv || {};

google.appengine.gv.init = function(apiRoot) {
  var apisToLoad;
  var callback = function() {
    if (--apisToLoad == 0) {
        angular.bootstrap(document, ['app']);
    }
  }

  apisToLoad = 1; // must match number of calls to gapi.client.load()
  gapi.client.load('consultantAPI', 'v1', callback, apiRoot);
};