
app.controller('StoreTaskCtrl', ['$scope', 'userSrv',
function($scope, userSrv) {

    $scope.task = {}
    $scope.task.status = 1
    $scope.clientList = [];
    $scope.loadedClients = false;

    $scope.companyStatusList = [];
    $scope.loadedStatusList = false;

    $scope.taskTypeList = [];
    $scope.loadedTaskTypes = false;

    $scope.client = {};
    $scope.selectedClient = undefined;
    $scope.clientTasks = [];
    $scope.showTimeLine = false;
    $scope.loadedTimeLine = false;
    $scope.disabled = undefined;
    $scope.searchEnabled = undefined;

    $scope.savingTask = false;

    //-----Datepicker
        var options = {
            weekday: "long", year: "numeric", month: "long",
            day: "numeric"
        };
        $scope.today = function() {
          $scope.task.taskDate = new Date().toLocaleDateString("es-CO", options);
          $scope.currentDate = new Date();
        };

        $scope.clear = function () {
          $scope.task.taskDate = null;
        };

        // Disable weekend selection
        $scope.disabled = function(date, mode) {
          return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
        };

        $scope.toggleMin = function() {
          $scope.minDate = $scope.minDate ? null : new Date();
        };
        $scope.toggleMin();

        $scope.open = function($event) {
          $event.preventDefault();
          $event.stopPropagation();

          $scope.opened = true;
        };

        $scope.dateOptions = {
          formatYear: 'yy',
          startingDay: 1,
          class: 'datepicker'
        };

        $scope.initDate = new Date();
        $scope.formats = ['dd MMMM yyyy'];
        $scope.format = $scope.formats[0];
    //-----End Datepicker
    $scope.enable = function() {
        $scope.disabled = false;
    };

    $scope.disable = function() {
        $scope.disabled = true;
    };

    $scope.enableSearch = function() {
        $scope.searchEnabled = true;
    }

    $scope.disableSearch = function() {
        $scope.searchEnabled = false;
    }
    $scope.init = function(){
        userSrv.checkSession();
        $scope.user = userSrv.getUser();
        gapi.client.consultantAPI.getConsultantClients({
                              'companyId': $scope.user.companyId,
                              'consultantId': $scope.user.consultantId
                              })
                 .execute(function(response) {
                     $scope.clientList = response.items;
                     $scope.loadedClients = true;
                     $scope.$apply();
                 });
        gapi.client.consultantAPI.getCompanyStatus({
                              'companyId': $scope.user.companyId
                              })
                 .execute(function(response) {
                     $scope.companyStatusList = response.items;
                     $scope.loadedStatusList = true;
                     $scope.$apply();
                 });
        gapi.client.consultantAPI.getCompanyTaskTypes({
                                      'companyId': $scope.user.companyId
                                      })
                         .execute(function(response) {
                             $scope.taskTypeList = response.items;
                             $scope.loadedTaskTypes = true;
                             $scope.$apply();
                         });
        document.getElementsByName('status')[0].checked = true;
    }

    $scope.getClientTasks = function(){
        if(!angular.isUndefined($scope.client.selected)){
            $scope.loadedTimeLine = false;
            gapi.client.consultantAPI.getClientTasks({
                                      'clientId': $scope.client.selected.clientId
                                      })
                         .execute(function(response) {
                             $scope.clientTasks = response.items;
                             $scope.showTimeLine = true;
                             $scope.loadedTimeLine = true;
                             $scope.$apply();
                         });
        }
    }

     $scope.$watch('client.selected', function(newClient, oldClient){
            $scope.getClientTasks();
            if(!angular.isUndefined(newClient))
                $scope.task.clientId = newClient.clientId;
        }, true);


    $scope.hasHour = function(task){
        taskDate = new Date(task.taskDate);
        return !(taskDate.getHours() == 0 && taskDate.getMinutes() == 0)

    }

    $scope.storeTask = function(task){
        $scope.savingTask = true;
        gapi.client.consultantAPI.storeTask(task)
        .execute(function(response) {
            $scope.getClientTasks();
            $scope.task.taskTypeId = undefined;
            $scope.task.status = 1
            $scope.task.taskDate = undefined;
            $scope.task.description = undefined;
            $scope.taskHour = '';
            document.getElementsByName('status')[0].checked = true;
            $scope.savingTask = false;
        });
    }

    $scope.submit = function(){
       if(
        !angular.isUndefined($scope.task.clientId) &&
        !angular.isUndefined($scope.task.taskTypeId) &&
        !angular.isUndefined($scope.task.status) &&
        !angular.isUndefined($scope.task.taskDate) &&
        !angular.isUndefined($scope.task.description)
       ){
            var taskDate = new Date($scope.task.taskDate);
            taskDate.setHours(0);
            taskDate.setMinutes(0);
            if(!angular.isUndefined($scope.taskHour)){
                var hoursMatch = $scope.taskHour.match(/\d{2}:\d{2}/);
                if(hoursMatch !=null){
                    hours = hoursMatch[0].split(":");
                    taskDate.setHours(hours[0]);
                    taskDate.setMinutes(hours[1]);
                }
            }
            $scope.task.taskDate = taskDate;
            $scope.task.consultantId = $scope.user.consultantId;
            $scope.task.companyId = $scope.user.companyId;
            $scope.task.order = 0;
            $scope.task.response = '';

            $scope.storeTask($scope.task);
       }
    }

    $scope.loadTask = function(task){
        $scope.task = task;
        document.getElementsByName('status')[task.status - 1].checked = true
        taskDate = new Date(task.taskDate);
        if (!(taskDate.getHours() == 0 && taskDate.getMinutes() == 0)){
            var hour = taskDate.getHours()<10?'0'+taskDate.getHours():taskDate.getHours();
            var minutes = taskDate.getMinutes()<10?'0'+taskDate.getHours():taskDate.getMinutes();
            $scope.taskHour = hour + ':' + minutes;
        }
    }
    $scope.init();
}]);
