
app.filter('hasHour', function(){
        return function(items, status){
            var out = [];
            angular.forEach(items, function(task) {
              taskDate = new Date(task.taskDate);
              if(
                  (
                      taskDate.getHours() == 0 &&
                      taskDate.getMinutes() == 0 &&
                      !status
                  )
                  ||
                  (
                      taskDate.getHours() != 0 &&
                      taskDate.getMinutes() != 0 &&
                      status
                  )

                  )
                  out.push(task)

            });
            return out;
        };
    });

app.controller('DashboardCtrl', ['$scope', '$http', '$anchorScroll', '$location', '$rootScope','userSrv', 'modalSrv',
function($scope, $http, $anchorScroll, $location, $rootScope, userSrv, modalSrv) {
    $scope.loadedTasks = false;
    $scope.hasTasks = true;

    //-----Datepicker
    var options = {
        weekday: "long", year: "numeric", month: "long",
        day: "numeric"
    };
    $scope.today = function() {
      $scope.dt = new Date().toLocaleDateString("es-CO", options);
      $scope.currentDate = new Date();
    };

    $scope.clear = function () {
      $scope.dt = null;
    };

    // Disable weekend selection
    $scope.disabled = function(date, mode) {
      return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
    };

    $scope.toggleMin = function() {
      $scope.minDate = $scope.minDate ? null : new Date();
    };
    $scope.toggleMin();

    $scope.open = function($event) {
      $event.preventDefault();
      $event.stopPropagation();

      $scope.opened = true;
    };

    $scope.dateOptions = {
      formatYear: 'yy',
      startingDay: 1,
      class: 'datepicker'
    };

    $scope.initDate = new Date();
    $scope.formats = ['dd MMMM yyyy'];
    $scope.format = $scope.formats[0];

    $scope.$watch('dt', function(newDate, oldDate){
        if(newDate instanceof Date){
            $scope.dt = newDate.toLocaleDateString("es-CO", options);
            $scope.currentDate = newDate;
            $scope.getConsultantTasks({
               'consultantId': $scope.user.consultantId,
               'taskDate': $scope.currentDate
               });
        }
    }, true);

    //-----End Datepicker

    $scope.taskList = []

    $scope.getAllTasks = function(){
        $scope.dt = "";
        $scope.getConsultantTasks({
                               'consultantId': $scope.user.consultantId
                               });
    }

    $scope.getConsultantTasks = function(options){
        $scope.loadedTasks = false;
        gapi.client.consultantAPI.getConsultantTasks(options)
         .execute(function(response) {
            if(!angular.isUndefined(response.items))
                $scope.taskList = response.items;
            $scope.loadedTasks = true;
            $scope.hasTasks = $scope.taskList.length > 0;
            $scope.$apply();
         });
    }

        $scope.init = function(){
           userSrv.checkSession();
           $scope.user = userSrv.getUser();
           $scope.today();
            $scope.getConsultantTasks({
              'consultantId': $scope.user.consultantId,
              'taskDate': $scope.currentDate
              });
         }
        $scope.init();

}]);