app.controller('AddClientCtrl', ['$scope', '$http', '$anchorScroll', '$location', '$rootScope', 'userSrv',
function($scope, $http, $anchorScroll, $location, $rootScope, userSrv) {
     userSrv.checkSession();
     $scope.client = {};
     $scope.visibleField = false;
     $scope.clientSources = null;
     $scope.user = null;
     $scope.savingClient = false;

     $scope.initUser = function(){
         userSrv.checkSession();
         $scope.user = userSrv.getUser();
         $scope.companySources = $scope.user.companySources;
         $scope.companyStatus = $scope.user.companyStatus;
     }

     //Run when loading directly from URL and not inside de app
     $scope.init = function () {
         $scope.initUser();
      }

     if(!angular.isUndefined(gapi.client)){
         $scope.init();
     }

      $scope.hideShow = function(){
        $scope.visibleField = !$scope.visibleField;
      }

      $scope.submit = function() {
          if ($scope.clientForm.$valid) {
             $scope.savingClient = true;
             $scope.client.clientLog = {'log':{"value": "Creación cliente"}, 'logDate':new Date()};
             $scope.client.consultantId = $scope.user.consultantId;
             $scope.client.companyId = $scope.user.companyId;
             $scope.client.enabled = true;
             gapi.client.consultantAPI.storeClient($scope.client)
             .execute(
                 function(resp) {
                   $scope.savingClient = false;
                   $scope.client.clientId = resp.keyId;
                   $scope.client.clientLog = null;
                   userSrv.pushClient($scope.client);
                   $location.path('manageclient/'+resp.keyId);
                   $scope.$apply();
             });
             }
         }

         $scope.setView = function(view) {
             $location.path(view);
         }
       }]);