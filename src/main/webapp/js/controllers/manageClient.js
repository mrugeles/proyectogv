app.controller('ManageClientCtrl', ['$scope', '$location','$stateParams', 'userSrv',
function($scope, $location, $stateParams, userSrv) {
    userSrv.checkSession();
    $scope.visibleField = false;
    $scope.clientSources = null;
    $scope.products = [];
    $scope.user = null;
    $scope.client = null;
    $scope.statusId = null;
    $scope.changeStatus = '';
    $scope.selectedTab = 1;
    $scope.forms = {};
    $scope.loadedStatusList = false;
    $scope.savingClient = false;
    $scope.task = {}
    $scope.task.status = 1
    $scope.taskTypeList = [];
    $scope.clientTasks = [];
    $scope.showTimeLine = false;
    $scope.loadedTimeLine = false;
    $scope.savingTask = false;
    $scope.taskActive = false;

    //-----Datepicker
        var options = {
            weekday: "long", year: "numeric", month: "long",
            day: "numeric"
        };
        $scope.today = function() {
          $scope.task.taskDate = new Date().toLocaleDateString("es-CO", options);
          $scope.currentDate = new Date();
        };

        $scope.clear = function () {
          $scope.task.taskDate = null;
        };

        // Disable weekend selection
        $scope.disabled = function(date, mode) {
          return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
        };

        $scope.toggleMin = function() {
          $scope.minDate = $scope.minDate ? null : new Date();
        };
        $scope.toggleMin();

        $scope.open = function($event) {
          $event.preventDefault();
          $event.stopPropagation();

          $scope.opened = true;
        };

        $scope.dateOptions = {
          formatYear: 'yy',
          startingDay: 1,
          class: 'datepicker'
        };

        $scope.initDate = new Date();
        $scope.formats = ['dd MMMM yyyy'];
        $scope.format = $scope.formats[0];
    //-----End Datepicker

    $scope.initUser = function(){
        userSrv.checkSession();
        $scope.user = userSrv.getUser();
    }

    $scope.getProductValue = function(productId){
        var productValue = '';
        for(var i = 0; i < $scope.clientProducts.length && productValue == ''; i++){
            productValue = $scope.clientProducts[i].companyProductId == productId?$scope.clientProducts[i].value:productValue;
        }
        return productValue;
    }

    $scope.getClientTasks = function(){
        $scope.loadedTimeLine = false;
        gapi.client.consultantAPI.getClientTasks({
                                  'clientId': $scope.client.clientId
                                  })
                     .execute(function(response) {
                         $scope.clientTasks = response.items;
                         $scope.showTimeLine = true;
                         $scope.loadedTimeLine = true;
                         if(!angular.isUndefined($stateParams.taskId)){
                             for(i = 0; i < $scope.clientTasks.length; i++){
                                if($scope.clientTasks[i].taskId == $stateParams.taskId){
                                    $scope.loadTask($scope.clientTasks[i]);
                                    break;
                                }
                             }
                             $scope.taskActive = true;
                         }
                         $scope.$apply();
                     });
        }

    $scope.init = function () {
        $scope.initUser();
        if(document.getElementById('ex1_value') != null){
            document.getElementById('ex1_value').value = '';
        }
        gapi.client.consultantAPI.getClient(
            {
                'companyId':$scope.user.companyId,
                'consultantId':$scope.user.consultantId,
                'clientId':$stateParams.clientId
            }).execute(
            function(resp) {
                 $scope.client = resp;
                 $scope.client.consultantId = $scope.user.consultantId;
                 $scope.client.companyId = $scope.user.companyId;
                 $scope.statusId = $scope.client.statusId;
                 if(angular.isUndefined($scope.client.clientLogs)){
                     $scope.client.clientLogs = [];
                 }
                 $scope.clientProducts =  angular.isUndefined($scope.client.productsValues)?[]:$scope.client.productsValues;
                 angular.forEach($scope.user.companyProducts, function(product, key){
                   product.value = $scope.getProductValue(product.companyProductId);
                 });
                 $scope.getClientTasks();
                 $scope.$apply();
               });
        gapi.client.consultantAPI.getCompanyStatus({
                                      'companyId': $scope.user.companyId
                                      })
                         .execute(function(response) {
                             $scope.companyStatusList = response.items;
                             $scope.$apply();
                         });
         gapi.client.consultantAPI.getCompanySources({
                                       'companyId': $scope.user.companyId
                                       })
                          .execute(function(response) {
                              $scope.companySources = response.items;
                              $scope.$apply();
                          });
         gapi.client.consultantAPI.getCompanyTaskTypes({
                                       'companyId': $scope.user.companyId
                                       })
                          .execute(function(response) {
                              $scope.taskTypeList = response.items;
                              $scope.loadedTaskTypes = true;
                              $scope.$apply();
                          });
        }

    if(!angular.isUndefined(gapi.client)){
        $scope.init();
    }

     $scope.hideShow = function(){
       $scope.visibleField = !$scope.visibleField;
     }

     $scope.onChangeStatus = function(){
        if($scope.statusId != $scope.client.statusId){
            var prevStatus = $scope.getStatusName($scope.statusId);
            var newStatus = $scope.getStatusName($scope.client.statusId);
            $scope.changeStatus = 'Cambio de estado de ['+ prevStatus +'] a ['+newStatus +']';
        }else{
            $scope.changeStatus = '';
        }
     }

     $scope.getStatusName = function(statusId){
             var statusName = '';
             for(var i = 0; i < $scope.user.clientStatus.length && statusName == ''; i++){
               if(statusId == $scope.user.clientStatus[i].statusId){
                 statusName = $scope.user.clientStatus[i].name;
               }
             }
             return statusName;
         }

     $scope.submitClientProducts = function() {
        if($scope.clientProducts.length > 0){
            var clientLog = {'log':{"value": 'Actualizacion de productos'}, 'logDate':new Date()};
            modalSrv.modal(true, 'Actualizando productos...');
            gapi.client.consultantAPI.storeClientProducts(
                    {
                     'clientId':$scope.client.clientId,
                     'companyId':$scope.user.companyId,
                     'productsValues': this.packProducts(),
                     'clientLog': clientLog,
                    }).execute(
                       function(resp) {
                             modalSrv.modal(false);
                             modalSrv.alert('Productos Actualizados');
                             $scope.client.clientLogs.splice(0, 0, clientLog);
                             $scope.$apply();
                         });
        }else{
            modalSrv.alert('No se han ingresado productos');
        }
     }

     $scope.packProducts = function(){
        var clientProducts = [];
        angular.forEach($scope.user.companyProducts, function(product, key){
            clientProducts.push({
                                    companyProductId:product.companyProductId,
                                    value:product.value
                                    });
         });

         return clientProducts;
     }
     $scope.submit = function() {
         if ($scope.forms.clientForm.$valid) {
            $scope.savingClient = true;
            $scope.client.note = $scope.client.note==null?"Actualización registro":$scope.client.note;
            $scope.client.note = $scope.changeStatus!=''?$scope.client.note + '. '+ $scope.changeStatus:$scope.client.note;
            var clientLog = {'log':{"value": $scope.client.note}, 'logDate':new Date()};
            $scope.client.clientLog = clientLog;
            gapi.client.consultantAPI.storeClient($scope.client).execute(
              function(resp) {
                    $scope.savingClient = false;
                    $scope.client.clientLogs.splice(0, 0, clientLog);
                    $scope.client.note = null;
                    $scope.$apply();
                });
            }
        }

        $scope.setView = function(view) {
            $location.path(view);
        }

        //Task functions
        $scope.hasHour = function(task){
                taskDate = new Date(task.taskDate);
                return !(taskDate.getHours() == 0 && taskDate.getMinutes() == 0)

            }

            $scope.storeTask = function(task){
                $scope.savingTask = true;
                gapi.client.consultantAPI.storeTask(task)
                .execute(function(response) {
                    $scope.getClientTasks();
                    $scope.task.taskTypeId = undefined;
                    $scope.task.status = 1
                    $scope.clear();
                    $scope.task.description = undefined;
                    $scope.taskHour = '';
                    document.getElementsByName('status')[0].checked = true;
                    $scope.savingTask = false;
                });
            }

            $scope.submitTask = function(){
               $scope.task.clientId = $stateParams.clientId;
               if(
                !angular.isUndefined($scope.task.taskTypeId) &&
                !angular.isUndefined($scope.task.status) &&
                !angular.isUndefined($scope.task.taskDate) &&
                !angular.isUndefined($scope.task.description)
               ){
                    var taskDate = new Date($scope.task.taskDate);
                    taskDate.setHours(0);
                    taskDate.setMinutes(0);
                    if(!angular.isUndefined($scope.taskHour)){
                        var hoursMatch = $scope.taskHour.match(/\d{2}:\d{2}/);
                        if(hoursMatch !=null){
                            hours = hoursMatch[0].split(":");
                            taskDate.setHours(hours[0]);
                            taskDate.setMinutes(hours[1]);
                        }
                    }
                    $scope.task.taskDate = taskDate;
                    $scope.task.consultantId = $scope.user.consultantId;
                    $scope.task.companyId = $scope.user.companyId;
                    $scope.task.order = 0;
                    $scope.task.response = '';

                    $scope.storeTask($scope.task);
               }
            }

            $scope.loadTask = function(task){
                $scope.task = task;
                document.getElementsByName('status')[task.status - 1].checked = true
                taskDate = new Date(task.taskDate);
                if (!(taskDate.getHours() == 0 && taskDate.getMinutes() == 0)){
                    var hour = taskDate.getHours()<10?'0'+taskDate.getHours():taskDate.getHours();
                    var minutes = taskDate.getMinutes()<10?'0'+taskDate.getMinutes():taskDate.getMinutes();
                    $scope.taskHour = hour + ':' + minutes;
                }
            }
}])