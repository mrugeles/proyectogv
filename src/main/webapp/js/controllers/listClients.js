app.controller('ListClientsCtrl', ['$scope', '$anchorScroll', '$location', 'userSrv',
function($scope, $anchorScroll, $location, userSrv) {

    userSrv.checkSession();
    $scope.loading = false;
    $scope.clientSources = [];
    $scope.clientStatus = [];
    $scope.clientList = [];
    $scope.next = null;
    $scope.showMoreButton = true;
    $scope.user = null;
    $scope.showList = false;

    $scope.selectedClient = function(selected) {
    if (selected) {
        window.location = '/#/app/manageClient/'+selected.originalObject.clientId;
    } else {
        console.log('cleared');
    }
    };

    $scope.initList = function(){
        $scope.clientList = [];
        $scope.next = null;
        $scope.showMoreButton = true;
        $scope.getClientList();
    }

    $scope.search = function(){
        $scope.initList();
        $scope.getClientList();
    }

    $scope.getClientList = function(){
        $scope.loading = true;
        gapi.client.consultantAPI.getClientsList(
            {
               companyId:$scope.user.companyId,
               consultantId:$scope.user.consultantId,
              'page': $scope.next
            }
        )
        .execute(
            function(response){
                if(!angular.isUndefined(response.clientsList)){
                     $scope.clientList.push.apply($scope.clientList, response.clientsList);
                }
                if($scope.next != response.page){
                    $scope.next = response.page;
                }else{
                    $scope.showMoreButton = false;
                }
                $scope.loading = false;
                //$scope.showList = true;
                $scope.$apply();
                $location.hash('bottom');
                $anchorScroll();
            }
        );
    }

    $scope.initUser = function(){
            userSrv.checkSession();
            $scope.user = userSrv.getUser();
     }

    //Run when loading directly from URL and not inside de app
    $scope.init = function () {
        $scope.initUser();
        $scope.initList();
     }

     if(!angular.isUndefined(gapi.client)){
         $scope.initUser();
     }
     $scope.init();
    }]);