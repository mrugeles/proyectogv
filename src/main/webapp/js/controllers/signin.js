'use strict';

/* Controllers */
  // signin controller
app.controller('SigninFormController', ['$scope', '$http','$window', '$cookies', '$rootScope', 'userSrv', 'modalSrv',
    function($scope, $http, $window, $cookies, $rootScope, userSrv, modalSrv) {
        $scope.loginValidation = false;

        $scope.submit = function() {
            $scope.loginValidation = false;
            //modalSrv.modal(true, "Autenticando Usuario");
            $cookies.put('user', null);
            if ($scope.loginForm.$valid) {
                gapi.client.consultantAPI.login(
                    {
                     'login': $scope.user.email,
                     'password': $scope.user.password
                     })
                     .execute(function(response) {
                         //modalSrv.modal(false);
                         if(response.result.result == "OK"){
                            userSrv.setUser(response.result.user);
                            userSrv.setClients(response.result.consultantClients);
                            $window.location.href = '#/';
                         }else{
                            $scope.authError = true;
                         }
                         $scope.$apply();
                     });
            }
        }
  }]);