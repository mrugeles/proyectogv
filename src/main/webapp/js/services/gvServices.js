var gvServices = angular.module('gvServices', []);

gvServices.factory('userSrv', ['$http', '$q', '$window', '$cookies','$rootScope',
    function($http, $q, $window, $cookies, $rootScope) {
        var userSrv = {};

        userSrv.logout = function(){
            $cookies.remove('user');
            $cookies.remove('client');
            userSrv.checkSession();
        }

        userSrv.getClient = function(){
            return $cookies.getObject('client');
        }

        userSrv.setClient = function(value){
            $rootScope.client = value;
            $cookies.putObject('client', value);
        }

        userSrv.setClients = function(clients){
            $rootScope.clients = clients;
            window.sessionStorage.setItem("clients", JSON.stringify(clients));
        }

        userSrv.getClients = function(clients){
            return JSON.parse(window.sessionStorage.getItem("clients"));
        }

        userSrv.pushClient = function(client){
            var clients = userSrv.getClients();
            clients.push(client);
            userSrv.setClients(clients)
        }

        userSrv.getUser = function(){
            return $cookies.getObject('user');
        }

        userSrv.setUser = function(value){
            if(angular.isUndefined(value.clients)){
                value.clients = [];
            }
            $rootScope.user = value;
            $cookies.putObject('user', value);
        }
        userSrv.checkSession = function(){
            if($cookies.getObject('user') == null){
                $window.location.href = '#/access/signin';
            }else{
                userSrv.setClients(userSrv.getClients());
                $('#userName').html($cookies.getObject('user').name);
                $('#container').show();
                $('#logoutBtn').click(function() {
                    userSrv.logout();
                });
            }
        }


        return userSrv;
    }
]);

gvServices.factory('modalSrv', ['$http', '$q', '$window', function($http, $q, $window) {
        var modalSrv = {};
        modalSrv.loading = function(show){
            if(show){
                $('#divView').hide();
                $('#loadingModal').show();
            }else{
                 $('#divView').show();
                 $('#loadingModal').hide();
            }
        }

        modalSrv.alert = function(message){
            $('#alertMessage').html(message==null?'':message);
            $('#alertModal').modal('show');
        }

        return modalSrv;
    }
]);
