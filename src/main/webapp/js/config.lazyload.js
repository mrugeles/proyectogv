// lazyload config

angular.module('app')
    /**
   * jQuery plugin config use ui-jq directive , config the js and css files that required
   * key: function name of the jQuery plugin
   * value: array of the css js file located
   */
  .constant('JQ_CONFIG', {
      easyPieChart:   [   '/js/jquery/jquery.easy-pie-chart/dist/jquery.easypiechart.fill.js'],
      sparkline:      [   '/js/jquery/jquery.sparkline/dist/jquery.sparkline.retina.js'],
      plot:           [   '/js/jquery/flot/jquery.flot.js',
                          '/js/jquery/flot/jquery.flot.pie.js',
                          '/js/jquery/flot/jquery.flot.resize.js',
                          '/js/jquery/flot.tooltip/js/jquery.flot.tooltip.min.js',
                          '/js/jquery/flot.orderbars/js/jquery.flot.orderBars.js',
                          '/js/jquery/flot-spline/js/jquery.flot.spline.min.js'],
      moment:         [   '/js/jquery/moment/moment.js'],
      screenfull:     [   '/js/jquery/screenfull/dist/screenfull.min.js'],
      slimScroll:     [   '/js/jquery/slimscroll/jquery.slimscroll.min.js'],
      sortable:       [   '/js/jquery/html5sortable/jquery.sortable.js'],
      nestable:       [   '/js/jquery/nestable/jquery.nestable.js',
                          '/js/jquery/nestable/jquery.nestable.css'],
      filestyle:      [   '/js/jquery/bootstrap-filestyle/src/bootstrap-filestyle.js'],
      slider:         [   '/js/jquery/bootstrap-slider/bootstrap-slider.js',
                          '/js/jquery/bootstrap-slider/bootstrap-slider.css'],
      chosen:         [   '/js/jquery/chosen/chosen.jquery.min.js',
                          '/js/jquery/chosen/bootstrap-chosen.css'],
      TouchSpin:      [   '/js/jquery/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js',
                          '/js/jquery/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css'],
      wysiwyg:        [   '/js/jquery/bootstrap-wysiwyg/bootstrap-wysiwyg.js',
                          '/js/jquery/bootstrap-wysiwyg/external/jquery.hotkeys.js'],
      dataTable:      [   '/js/jquery/datatables/media/js/jquery.dataTables.min.js',
                          '/js/jquery/plugins/integration/bootstrap/3/dataTables.bootstrap.js',
                          '/js/jquery/plugins/integration/bootstrap/3/dataTables.bootstrap.css'],
      vectorMap:      [   '/js/jquery/bower-jvectormap/jquery-jvectormap-1.2.2.min.js',
                          '/js/jquery/bower-jvectormap/jquery-jvectormap-world-mill-en.js',
                          '/js/jquery/bower-jvectormap/jquery-jvectormap-us-aea-en.js',
                          '/js/jquery/bower-jvectormap/jquery-jvectormap.css'],
      footable:       [   '/js/jquery/footable/v3/js/footable.min.js',
                          '/js/jquery/footable/v3/css/footable.bootstrap.min.css'],
      fullcalendar:   [   '/js/jquery/moment/moment.js',
                          '/js/jquery/fullcalendar/dist/fullcalendar.min.js',
                          '/js/jquery/fullcalendar/dist/fullcalendar.css',
                          '/js/jquery/fullcalendar/dist/fullcalendar.theme.css'],
      daterangepicker:[   '/js/jquery/moment/moment.js',
                          '/js/jquery/bootstrap-daterangepicker/daterangepicker.js',
                          '/js/jquery/bootstrap-daterangepicker/daterangepicker-bs3.css'],
      tagsinput:      [   '/js/jquery/bootstrap-tagsinput/dist/bootstrap-tagsinput.js',
                          '/js/jquery/bootstrap-tagsinput/dist/bootstrap-tagsinput.css'],
      clockpicker:    [   '/js/jquery/clockpicker-gh-pages/dist/bootstrap-clockpicker.min.js',
                          '/js/jquery/clockpicker-gh-pages/dist/bootstrap-clockpicker.min.css']

    }
  )
  .constant('MODULE_CONFIG', [
      {
          name: 'ngGrid',
          files: [
              '/js/angular/ng-grid/build/ng-grid.min.js',
              '/js/angular/ng-grid/ng-grid.min.css',
              '/js/angular/ng-grid/ng-grid.bootstrap.css'
          ]
      },
      {
          name: 'ui.grid',
          files: [
              '/js/angular/angular-ui-grid/ui-grid.min.js',
              '/js/angular/angular-ui-grid/ui-grid.min.css',
              '/js/angular/angular-ui-grid/ui-grid.bootstrap.css'
          ]
      },
      {
          name: 'ui.select',
          files: [
              '/js/angular/angular-ui-select/dist/select.min.js',
              '/js/angular/angular-ui-select/dist/select.min.css'
          ]
      },
      {
          name:'angularFileUpload',
          files: [
            '/js/angular/angular-file-upload/angular-file-upload.js'
          ]
      },
      {
          name:'ui.calendar',
          files: ['/js/angular/angular-ui-calendar/src/calendar.js']
      },
      {
          name: 'ngImgCrop',
          files: [
              '/js/angular/ngImgCrop/compile/minified/ng-img-crop.js',
              '/js/angular/ngImgCrop/compile/minified/ng-img-crop.css'
          ]
      },
      {
          name: 'angularBootstrapNavTree',
          files: [
              '/js/angular/angular-bootstrap-nav-tree/dist/abn_tree_directive.js',
              '/js/angular/angular-bootstrap-nav-tree/dist/abn_tree.css'
          ]
      },
      {
          name: 'toaster',
          files: [
              '/js/angular/angularjs-toaster/toaster.js',
              '/js/angular/angularjs-toaster/toaster.css'
          ]
      },
      {
          name: 'textAngular',
          files: [
              '/js/angular/textAngular/dist/textAngular-sanitize.min.js',
              '/js/angular/textAngular/dist/textAngular.min.js'
          ]
      },
      {
          name: 'vr.directives.slider',
          files: [
              '/js/angular/venturocket-angular-slider/build/angular-slider.min.js',
              '/js/angular/venturocket-angular-slider/build/angular-slider.css'
          ]
      },
      {
          name: 'com.2fdevs.videogular',
          files: [
              '/js/angular/videogular/videogular.min.js'
          ]
      },
      {
          name: 'com.2fdevs.videogular.plugins.controls',
          files: [
              '/js/angular/videogular-controls/controls.min.js'
          ]
      },
      {
          name: 'com.2fdevs.videogular.plugins.buffering',
          files: [
              '/js/angular/videogular-buffering/buffering.min.js'
          ]
      },
      {
          name: 'com.2fdevs.videogular.plugins.overlayplay',
          files: [
              '/js/angular/videogular-overlay-play/overlay-play.min.js'
          ]
      },
      {
          name: 'com.2fdevs.videogular.plugins.poster',
          files: [
              '/js/angular/videogular-poster/poster.min.js'
          ]
      },
      {
          name: 'com.2fdevs.videogular.plugins.imaads',
          files: [
              '/js/angular/videogular-ima-ads/ima-ads.min.js'
          ]
      },
      {
          name: 'xeditable',
          files: [
              '/js/angular/angular-xeditable/dist/js/xeditable.min.js',
              '/js/angular/angular-xeditable/dist/css/xeditable.css'
          ]
      },
      {
          name: 'smart-table',
          files: [
              '/js/angular/angular-smart-table/dist/smart-table.min.js'
          ]
      },
      {
          name: 'angular-skycons',
          files: [
              '/js/angular/angular-skycons/angular-skycons.js'
          ]
      },
      {
          name: 'angucomplete-alt',
          files: [
                '/js/angular/angucomplete-alt/angucomplete-alt.min.js'
          ]
      }
    ]
  )
  // oclazyload config
  .config(['$ocLazyLoadProvider', 'MODULE_CONFIG', function($ocLazyLoadProvider, MODULE_CONFIG) {
      // We configure ocLazyLoad to use the lib script.js as the async loader
      $ocLazyLoadProvider.config({
          debug:  false,
          events: true,
          modules: MODULE_CONFIG
      });
  }])
;
