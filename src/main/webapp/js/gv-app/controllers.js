﻿'use strict';

/* Controllers */

var gvControllers = angular.module('gvControllers', []);

gvControllers.controller('HomeCtrl', ['$scope', '$http', 'userSrv', 'modalSrv',
  function ($scope, $http, userSrv, modalSrv) {
        $scope.user = null;
        $scope.init = function(){
            userSrv.checkSession();
            modalSrv.modal(false);
            $scope.user = userSrv.getUser();
        }
  } ]);


gvControllers.controller('AddClientCtrl', ['$scope', '$http', '$location', 'userSrv', 'modalSrv',
  function ($scope, $http, $location, userSrv, modalSrv) {
    userSrv.checkSession();
    modalSrv.modal(true, 'Un momento por favor...');
    $scope.visibleField = false;
    $scope.clientSources = null;
    $scope.user = null;

    $scope.initUser = function(){
        userSrv.checkSession();
        $scope.user = userSrv.getUser();
        $scope.clientSources = $scope.user.clientSources;
        $scope.clientStatusList = $scope.user.clientStatus;
    }

    //Run when loading directly from URL and not inside de app
    $scope.init = function () {
        $scope.initUser();
        modalSrv.modal(false);
     }

    if(!angular.isUndefined(gapi.client)){
        $scope.init();
    }

     $scope.hideShow = function(){
       $scope.visibleField = !$scope.visibleField;
     }

     $scope.submit = function() {
         if ($scope.clientForm.$valid) {
            $scope.loadingMessage = 'Guardando..';
            var clientLog = {'log':{"value": "Creación cliente"}, 'logDate':new Date()};
            $('#loadingModal').modal('show');
            gapi.client.consultantAPI.storeClient(
                {
                 'enabled':true,
                 'companyId':$scope.user.companyId,
                 'consultantId':$scope.user.consultantId,
                 'sourceId': $scope.clientSource,
                 'statusId': $scope.clientStatus,
                 'clientName': $scope.clientName,
                 'contactName': $scope.contactName,
                 'documentType': $scope.documentType,
                 'documentNumber': $scope.documentNumber,
                 'phoneNumber': $scope.phoneNumber,
                 'phoneNumber2': $scope.phoneNumber2,
                 'address': $scope.address,
                 'email': $scope.email,
                 'clientLog': clientLog
                }
            ).execute(
                function(resp) {
                  $scope.user.clients.push(
                  {
                      clientId: resp.keyId,
                      clientName: $scope.clientName,
                      companyId: $scope.user.companyId,
                      consultantId: $scope.user.consultantId
                  });
                  userSrv.setUser($scope.user);
                  $location.path('manageclient/'+resp.keyId);
                  $scope.$apply();
            });
            }
        }

        $scope.setView = function(view) {
            $location.path(view);
        }
      }
   ]);

gvControllers.controller('ManageClientCtrl', ['$scope', '$http', '$location', '$routeParams', 'userSrv', 'modalSrv',
  function ($scope, $http, $location, $routeParams, userSrv, modalSrv) {
    userSrv.checkSession();
    modalSrv.modal(true, 'Cargando información del cliente');
    $scope.visibleField = false;
    $scope.clientSources = null;
    $scope.products = [];
    $scope.user = null;
    $scope.client = null;
    $scope.statusId = null;
    $scope.changeStatus = '';
    $scope.selectedTab = 1;
    $scope.forms = {};

    $scope.initUser = function(){
        userSrv.checkSession();
        $scope.user = userSrv.getUser();
    }

    $scope.getProductValue = function(productId){
        var productValue = '';
        for(var i = 0; i < $scope.clientProducts.length && productValue == ''; i++){
            productValue = $scope.clientProducts[i].companyProductId == productId?$scope.clientProducts[i].value:productValue;
        }
        return productValue;
    }

    $scope.init = function () {
        $scope.initUser();
        gapi.client.consultantAPI.getClient(
            {
                'companyId':$scope.user.companyId,
                'consultantId':$scope.user.consultantId,
                'clientId':$routeParams.clientId
            }).execute(
            function(resp) {
                 $scope.client = resp;
                 $scope.statusId = $scope.client.statusId;
                 if(angular.isUndefined($scope.client.clientLogs)){
                     $scope.client.clientLogs = [];
                 }
                 $scope.clientProducts =  angular.isUndefined($scope.client.productsValues)?[]:$scope.client.productsValues;
                 angular.forEach($scope.user.companyProducts, function(product, key){
                   product.value = $scope.getProductValue(product.companyProductId);
                 });
                 modalSrv.modal(false);
                 $scope.$apply();
               });
     }

    if(!angular.isUndefined(gapi.client)){
        $scope.init();
    }

     $scope.hideShow = function(){
       $scope.visibleField = !$scope.visibleField;
     }

     $scope.onChangeStatus = function(){
        if($scope.statusId != $scope.client.statusId){
            var prevStatus = $scope.getStatusName($scope.statusId);
            var newStatus = $scope.getStatusName($scope.client.statusId);
            $scope.changeStatus = 'Cambio de estado de ['+ prevStatus +'] a ['+newStatus +']';
        }else{
            $scope.changeStatus = '';
        }
     }

     $scope.getStatusName = function(statusId){
             var statusName = '';
             for(var i = 0; i < $scope.user.clientStatus.length && statusName == ''; i++){
               if(statusId == $scope.user.clientStatus[i].statusId){
                 statusName = $scope.user.clientStatus[i].name;
               }
             }
             return statusName;
         }

     $scope.submitClientProducts = function() {
        if($scope.clientProducts.length > 0){
            var clientLog = {'log':{"value": 'Actualizacion de productos'}, 'logDate':new Date()};
            modalSrv.modal(true, 'Actualizando productos...');
            gapi.client.consultantAPI.storeClientProducts(
                    {
                     'clientId':$scope.client.clientId,
                     'companyId':$scope.user.companyId,
                     'productsValues': this.packProducts(),
                     'clientLog': clientLog,
                    }).execute(
                       function(resp) {
                             modalSrv.modal(false);
                             modalSrv.alert('Productos Actualizados');
                             $scope.client.clientLogs.splice(0, 0, clientLog);
                             $scope.$apply();
                         });
        }else{
            modalSrv.alert('No se han ingresado productos');
        }
     }

     $scope.packProducts = function(){
        var clientProducts = [];
        angular.forEach($scope.user.companyProducts, function(product, key){
            clientProducts.push({
                                    companyProductId:product.companyProductId,
                                    value:product.value
                                    });
         });

         return clientProducts;
     }
     $scope.submit = function() {
         if ($scope.forms.clientForm.$valid) {
            $scope.client.note = $scope.client.note==null?"Actualización registro":$scope.client.note;
            $scope.client.note = $scope.changeStatus!=''?$scope.client.note + '. '+ $scope.changeStatus:$scope.client.note;
            var clientLog = {'log':{"value": $scope.client.note}, 'logDate':new Date()};
            modalSrv.modal(true, 'Guardando...');
            gapi.client.consultantAPI.storeClient(
                {
                 'clientId':$scope.client.clientId,
                 'companyId':$scope.user.companyId,
                 'consultantId':$scope.user.consultantId,
                 'sourceId': $scope.client.sourceId,
                 'statusId': $scope.client.statusId,
                 'enabled': $scope.client.enabled,
                 'clientName': $scope.client.clientName,
                 'contactName': $scope.client.contactName,
                 'phoneNumber': $scope.client.phoneNumber,
                 'documentType': $scope.client.documentType == "0"?null:$scope.client.documentType,
                 'documentNumber': $scope.client.documentNumber ==""?null:$scope.client.documentNumber,
                 'phoneNumber2': $scope.client.phoneNumber2 == ""?null:$scope.client.phoneNumber2,
                 'address': $scope.client.address==""?null:$scope.client.address,
                 'email': $scope.client.email==""?null:$scope.client.email,
                 'clientLog': clientLog,
                }
            ).execute(
              function(resp) {
                    modalSrv.modal(false);
                    modalSrv.alert('Cambios guardados');
                    $scope.client.clientLogs.splice(0, 0, clientLog);
                    $scope.client.note = null;
                    $scope.$apply();
                });
            }
        }

        $scope.setView = function(view) {
            $location.path(view);
        }
      }
   ]);


gvControllers.controller('ManageClientsCtrl', ['$scope', '$http', '$anchorScroll', '$location', 'userSrv', 'modalSrv',
  function ($scope, $http, $anchorScroll, $location, userSrv, modalSrv) {
    userSrv.checkSession();
    $scope.loading = false;
    $scope.clientSources = [];
    $scope.clientStatus = [];
    $scope.clientList = [];
    $scope.clients = null;
    $scope.next = null;
    $scope.showMoreButton = true;
    $scope.user = null;
    $scope.showList = false;

    $scope.selectedClient = function(selected) {
          if (selected) {
           $location.path('manageclient/'+selected.originalObject.clientId);
          } else {
            console.log('cleared');
          }
        };

    $scope.initUser = function(){
        userSrv.checkSession();
        $scope.user = userSrv.getUser();
        $scope.clientSources = $scope.user.clientSources;
        $scope.clientStatus = $scope.user.clientStatus;
        $scope.clients = $scope.user.clients;
     }

    //Run when loading directly from URL and not inside de app
    $scope.init = function () {
        $scope.initUser();
        modalSrv.modal(false);
        $scope.$apply();
     }

    if(!angular.isUndefined(gapi.client)){
        $scope.initUser();
    }

    $scope.initList = function(){
        $scope.clientList = [];
        $scope.next = null;
        $scope.showMoreButton = true;
    }

    $scope.search = function(){
        $scope.initList();
        $scope.getClientList();
    }

    $scope.getStatusName = function(statusId){
        var statusName = '';
        for(var i = 0; i < $scope.user.clientStatus.length && statusName == ''; i++){
          if(statusId == $scope.user.clientStatus[i].statusId){
            statusName = $scope.user.clientStatus[i].name;
          }
        }
        return statusName;
    }

    $scope.getClientList = function(){
        modalSrv.modal(true,'Buscando...');
        gapi.client.consultantAPI.getClientsList(
                    {
                       companyId:$scope.user.companyId,
                       consultantId:$scope.user.consultantId,
                      'page': $scope.next
                    }
                    )
                    .execute(
                        function(response){
                            if(!angular.isUndefined(response.clientsList)){
                                 $scope.clientList.push.apply($scope.clientList, response.clientsList);
                            }
                            if($scope.next != response.page){
                                $scope.next = response.page;
                            }else{
                                $scope.showMoreButton = false;
                            }
                            modalSrv.modal(false);
                            $scope.showList = true;
                            $scope.$apply();
                            $location.hash('bottom');
                            $anchorScroll();

                        }
                    );
    }

  } ]);


