'use strict';

var gvApp = angular.module('gvApp', [
  'ngRoute',
  'ngCookies',
  'ngTouch',
  'angucomplete-alt',
  'toggle-switch',
  'gvServices',
  'gvControllers'
]);

gvApp.config(['$routeProvider',
  function ($routeProvider) {
      $routeProvider.
      when('/', {
          templateUrl: 'views/home.html',
          controller: 'HomeCtrl'
      }).
      when('/addclient', {
          templateUrl: 'views/addClient.html',
          controller: 'AddClientCtrl'
      }).
      when('/manageclient/:clientId', {
          templateUrl: 'views/manageClient/manageClient.html',
          controller: 'ManageClientCtrl'
      }).
      when('/manageclients', {
          templateUrl: 'views/manageClients.html',
          controller: 'ManageClientsCtrl'
      }).
      otherwise({
          redirectTo: '/'
      });
  } ]);