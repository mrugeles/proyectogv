var gvServices = angular.module('gvServices', []);

gvServices.factory('userSrv', ['$http', '$q', '$window', '$cookies','$rootScope',
    function($http, $q, $window, $cookies, $rootScope) {
        var userSrv = {};

        userSrv.logout = function(){
            $cookies.remove('user');
            $cookies.remove('client');
            userSrv.checkSession();
        }

        userSrv.getClient = function(){
            return $cookies.getObject('client');
        }

        userSrv.setClient = function(value){
            $rootScope.client = value;
            $cookies.putObject('client', value);
        }

        userSrv.getUser = function(){
            return $cookies.getObject('user');
        }

        userSrv.setUser = function(value){
            if(angular.isUndefined(value.clients)){
                value.clients = [];
            }
            $rootScope.user = value;
            $cookies.putObject('user', value);
        }
        userSrv.checkSession = function(){
            if($cookies.getObject('user') == null){
                $window.location.href = '/login.html';
            }else{
                $('#userName').html($cookies.getObject('user').name);
                $('#container').show();
                $('#logoutBtn').click(function() {
                    userSrv.logout();
                });
            }
        }


        return userSrv;
    }
]);

gvServices.factory('modalSrv', ['$http', '$q', '$window', function($http, $q, $window) {
        var modalSrv = {};
        modalSrv.modal = function(show, message){
            $('#modalMessage').html(message==null?'':message);
            $('#loadingModal').modal(show?'show':'hide');
        }

        modalSrv.alert = function(message){
            $('#alertMessage').html(message==null?'':message);
            $('#alertModal').modal('show');
        }

        return modalSrv;
    }
]);
