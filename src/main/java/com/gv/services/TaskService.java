package com.gv.services;

import com.google.appengine.api.datastore.*;
import com.gv.beans.KeyBean;
import com.gv.beans.Task;
import com.gv.beans.TaskType;

import java.util.*;


public class TaskService {

    private CommonService commonService = CommonService.getInstance();
    private static TaskService instance;

    public static TaskService getInstance() {
        if(instance == null) {
            instance = new TaskService();
        }
        return instance;
    }

    public KeyBean storeTask(Task task) throws EntityNotFoundException {
        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Entity entity = commonService.getDSEntity("Task", task.getTaskId(), "Client", task.getClientId());
        entity.setProperty("companyId", task.getCompanyId());
        entity.setProperty("clientId", task.getClientId());
        entity.setProperty("consultantId", task.getConsultantId());
        entity.setProperty("taskTypeId", task.getTaskTypeId());
        entity.setProperty("taskDate", task.getTaskDate());
        entity.setProperty("description", task.getDescription());
        entity.setProperty("response", task.getResponse());
        entity.setProperty("order", task.getOrder());
        entity.setProperty("status", task.getStatus());
        Key entityKey = datastore.put(entity);
        KeyBean keyBean = new KeyBean(entityKey.getId());
        return keyBean;
    }

    public KeyBean storeTaskType(TaskType taskType) throws EntityNotFoundException {
        Key companyKey = KeyFactory.createKey("Company", taskType.getCompanyId());
        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Entity entity = commonService.getDSEntity ("TaskType", taskType.getId(), "Company", taskType.getCompanyId());
        entity.setProperty("name", taskType.getName());
        entity.setProperty("companyId", companyKey.getId());
        return new KeyBean(datastore.put(entity).getId());
    }

    public Task getTask(){
        return null;
    }

    public List<Task> getConsultantTasks(Task task) {
        List<Task> taskList = new ArrayList<Task>();

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Query.Filter consultantFilter =
                new Query.FilterPredicate("consultantId",
                        Query.FilterOperator.EQUAL,
                        task.getConsultantId());

        Query.Filter filterDateFrom = null;
        Query.Filter filterDateTo = null;
        if(task.getTaskDate() != null) {
            Calendar calendarFrom = Calendar.getInstance();
            Calendar calendarTo = Calendar.getInstance();
            calendarFrom.setTime(task.getTaskDate());
            calendarFrom.set(Calendar.HOUR_OF_DAY, 0);
            calendarFrom.set(Calendar.MINUTE, 0);
            calendarFrom.set(Calendar.SECOND, 0);

            calendarTo.setTime(calendarFrom.getTime());
            calendarTo.add(Calendar.HOUR_OF_DAY, 24);

            filterDateFrom = new Query.FilterPredicate("taskDate",
                    Query.FilterOperator.GREATER_THAN_OR_EQUAL,
                    calendarFrom.getTime());
            filterDateTo = new Query.FilterPredicate("taskDate",
                    Query.FilterOperator.LESS_THAN,
                    calendarTo.getTime());
            System.out.println("calendarFrom = " + calendarFrom.getTime());
            System.out.println("calendarTo = " + calendarTo.getTime());
        }

        Query.Filter statusFilter = null;
        if(task.getStatus() != null){
            statusFilter =
                    new Query.FilterPredicate("status",
                            Query.FilterOperator.EQUAL,
                            task.getStatus());
        }

        Query q = new Query("Task");

        if(task.getTaskDate() != null && task.getStatus() == null) {
            Query.Filter filter =
                    Query.CompositeFilterOperator.and(consultantFilter, filterDateFrom, filterDateTo);
            q.setFilter(filter);

        }else if(task.getStatus() != null && task.getTaskDate() == null){
            Query.Filter filter =
                    Query.CompositeFilterOperator.and(consultantFilter, statusFilter);
            q.setFilter(filter);
        }else if(task.getStatus() != null && task.getTaskDate() != null){
            Query.Filter filter =
                    Query.CompositeFilterOperator.and(consultantFilter, filterDateFrom, filterDateTo, statusFilter);
            q.setFilter(filter);
        }else{
            q = new Query("Task").setFilter(consultantFilter);
        }
        q.addSort("taskDate", Query.SortDirection.DESCENDING);

        PreparedQuery pq = datastore.prepare(q);
        Iterator<Entity> entityIterator = pq.asIterator();
        while(entityIterator.hasNext()){
            taskList.add(this.toTask(entityIterator.next()));
        }
        return taskList;
    }

    public List<Task> getClientTasks(Task task) {
        List<Task> taskList = new ArrayList<Task>();

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Query.Filter clientFilter =
                new Query.FilterPredicate("clientId",
                        Query.FilterOperator.EQUAL,
                        task.getClientId());



        Query q = new Query("Task").setFilter(clientFilter);
        q.addSort("taskDate", Query.SortDirection.ASCENDING);

        PreparedQuery pq = datastore.prepare(q);
        Iterator<Entity> entityIterator = pq.asIterator();
        while(entityIterator.hasNext()){
            taskList.add(this.toTask(entityIterator.next()));
        }
        return taskList;
    }

    public List<TaskType>getCompanyTaskTypes(Long companyId){
        List<TaskType> taskTypesList = new ArrayList<>();

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Query.Filter companyFilter =
                new Query.FilterPredicate("companyId",
                        Query.FilterOperator.EQUAL,
                        companyId);
        Query q = new Query("TaskType");
        q.setFilter(companyFilter);

        PreparedQuery pq = datastore.prepare(q);
        Iterator<Entity> entityIterator = pq.asIterator();
        while(entityIterator.hasNext()){
            taskTypesList.add(this.toTaskType(entityIterator.next()));
        }
        return taskTypesList;
    }

    private Task toTask(Entity entity) {
        Task task = new Task();
        task.setTaskId(entity.getKey().getId());
        task.setCompanyId(Long.parseLong(entity.getProperty("companyId").toString()));
        task.setClientId(Long.parseLong(entity.getProperty("clientId").toString()));
        task.setConsultantId(Long.parseLong(entity.getProperty("consultantId").toString()));
        task.setDescription(entity.getProperty("description").toString());
        task.setResponse(entity.getProperty("response").toString());
        task.setOrder(Integer.parseInt(entity.getProperty("order").toString()));
        task.setStatus(Integer.parseInt(entity.getProperty("status").toString()));
        task.setTaskDate((Date)entity.getProperty("taskDate"));
        task.setTaskTypeId(Long.parseLong(entity.getProperty("taskTypeId").toString()));

        Entity entityTaskType = null;
        try {
            entityTaskType = commonService.getDSEntity ("TaskType", task.getTaskTypeId(), "Company", task.getCompanyId());
            task.setTaskTypeName(entityTaskType.getProperty("name").toString());

            Entity entityClient = commonService.getDSEntity("Client", task.getClientId(), "Company", task.getCompanyId());
            task.setClientName(entityClient.getProperty("clientName").toString());
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
        }

        return task;
    }

    private TaskType toTaskType(Entity entity) {
        TaskType taskType = new TaskType();
        taskType.setId(entity.getKey().getId());
        taskType.setCompanyId(Long.parseLong(entity.getProperty("companyId").toString()));
        taskType.setName(entity.getProperty("name").toString());
        return taskType;
    }
}
