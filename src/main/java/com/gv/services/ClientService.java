package com.gv.services;

import com.google.appengine.api.datastore.*;
import com.gv.beans.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class ClientService {

    private CommonService commonService = CommonService.getInstance();
    private static ClientService instance;

    public static ClientService getInstance() {
        if(instance == null) {
            instance = new ClientService();
        }
        return instance;
    }

    public KeyBean storeClient(Client client) throws EntityNotFoundException {
        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Entity entity = commonService.getDSEntity("Client", client.getClientId(), "Company", client.getCompanyId());
        entity.setProperty("consultantId", client.getConsultantId());
        entity.setProperty("sourceId", client.getSourceId());
        entity.setProperty("statusId", client.getStatusId());
        entity.setProperty("enabled", client.isEnabled());
        entity.setProperty("clientName", client.getClientName());
        entity.setProperty("contactName", client.getContactName());
        entity.setProperty("documentType", client.getDocumentType());
        entity.setProperty("documentNumber", client.getDocumentNumber());
        entity.setProperty("phoneNumber", client.getPhoneNumber());
        entity.setProperty("phoneNumber2", client.getPhoneNumber2());
        entity.setProperty("address", client.getAddress());
        entity.setProperty("email", client.getEmail());
        Key entityKey = datastore.put(entity);
        storeClientLog(client.getClientLog(), entityKey);
        KeyBean keyBean = new KeyBean(entityKey.getId());
        return keyBean;
    }

    public Client getClient(Client client) throws EntityNotFoundException {
        Entity entity = commonService.getDSEntity ("Client", client.getClientId(), "Company", client.getCompanyId());
        return this.toClient(entity);
    }


    public ClientsList getClientsList(ClientListQuery clientListQuery){
        List<Client> list = new ArrayList<Client>();
        QueryResultList<Entity> results = this.getClientListQueryResult(clientListQuery);
        for (Entity entity : results) {
            try {
                Client client = this.toClient(entity);
                Entity statusEntity = commonService.getDSEntity("CompanyStatus", Long.parseLong(entity.getProperty("statusId").toString()), "Company", client.getCompanyId());
                client.setClientStatus(statusEntity.getProperty("name").toString());

                list.add(client);
            } catch (EntityNotFoundException e) {
                e.printStackTrace();
            }
        }

        ClientsList clientsList = new ClientsList(results.getCursor().toWebSafeString(), list);
        return clientsList;
    }


    public KeyBean storeClientProducts(Client client) throws EntityNotFoundException {
        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Key companyKey = KeyFactory.createKey("Company", client.getCompanyId());
        Key clientKey = new KeyFactory.Builder(companyKey)
                .addChild("Client", client.getClientId()).getKey();

        for (ClientProductValue clientProductValue:client.getProductsValues()) {
            Entity clientProductEntity = getClientProductValueEntity(clientKey, clientProductValue, datastore);
            if(clientProductValue.getValue() != null) {
                clientProductEntity.setProperty("companyProductId", clientProductValue.getCompanyProductId());
                clientProductEntity.setProperty("value", clientProductValue.getValue());
                datastore.put(clientProductEntity);
            }else{
                try {
                    datastore.delete(clientProductEntity.getKey());
                }catch (Exception ex){}
            }
        }

        storeClientLog(client.getClientLog(), clientKey);
        KeyBean keyBean = new KeyBean(new Long(1));
        return keyBean;
    }

    public List<BaseClient> getConsultantClients(Long companyId, Long consultantId){
        List<BaseClient> clientList = new ArrayList<BaseClient>();
        Key companyKey = KeyFactory.createKey("Company", companyId);
        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Query.Filter consultantFilter =
                new Query.FilterPredicate("consultantId",
                        Query.FilterOperator.EQUAL,
                        consultantId);
        Query q = new Query("Client")
                .setAncestor(companyKey)
                .setFilter(consultantFilter)
                .addSort("clientName", Query.SortDirection.ASCENDING);
        PreparedQuery pq = datastore.prepare(q);
        Iterator<Entity> entityIterator = pq.asIterator();
        while(entityIterator.hasNext()){
            clientList.add(this.toBaseClient(entityIterator.next()));
        }
        return clientList;
    }

    public void storeClientLog(ClientLog clientLog, Key clientKey) throws EntityNotFoundException {
        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Entity entity = new Entity("ClientLog", clientKey);
        entity.setProperty("log", clientLog.getLog());
        entity.setProperty("logDate", clientLog.getLogDate());
        datastore.put(entity);
    }

    public List<ClientLog> getClientLogs(Key clientKey){
        List<ClientLog> clientLogs = new ArrayList<ClientLog>();
        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Query q = new Query("ClientLog")
                .setAncestor(clientKey)
                .addSort("logDate", Query.SortDirection.DESCENDING);
        PreparedQuery pq = datastore.prepare(q);
        Iterator<Entity> entityIterator = pq.asIterator();
        while(entityIterator.hasNext()){
            clientLogs.add(this.toClientLog(entityIterator.next()));
        }

        return clientLogs;
    }

    /*********************Private Methods*********************************/
    private Client toClient(Entity entity) throws EntityNotFoundException {
        Client client = new Client();
        client.setClientId(entity.getKey().getId());
        client.setCompanyId(entity.getKey().getParent().getId());
        client.setConsultantId((Long) entity.getProperty("consultantId"));
        client.setSourceId((Long)entity.getProperty("sourceId"));
        client.setStatusId((Long)entity.getProperty("statusId"));
        client.setEnabled((boolean)(entity.getProperty("enabled")==null?true:entity.getProperty("enabled")));
        client.setClientName((String)entity.getProperty("clientName"));
        client.setContactName((String)entity.getProperty("contactName"));
        client.setDocumentType((String) entity.getProperty("documentType"));
        client.setDocumentNumber((String) entity.getProperty("documentNumber"));
        client.setPhoneNumber((String)entity.getProperty("phoneNumber"));
        client.setPhoneNumber2((String)entity.getProperty("phoneNumber2"));
        client.setAddress((String)entity.getProperty("address"));
        client.setEmail((String)entity.getProperty("email"));
        client.setClientLogs(getClientLogs(entity.getKey()));
        client.setProductsValues(getProductValues(entity.getKey()));

        return client;
    }

    private ClientLog toClientLog(Entity entity){
        ClientLog clientLog = new ClientLog();
        clientLog.setLog((Text) entity.getProperty("log"));
        clientLog.setLogDate((Date) entity.getProperty("logDate"));
        return clientLog;
    }

    private List<ClientProductValue> getProductValues(Key clientKey) {
        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        List<ClientProductValue> clientProductValues = new ArrayList<ClientProductValue>();
        Query q = new Query("ClientProductValue").setAncestor(clientKey);
        PreparedQuery pq = datastore.prepare(q);
        Iterator<Entity> entityIterator = pq.asIterator();
        while(entityIterator.hasNext()){
            clientProductValues.add(this.toProductValue(entityIterator.next()));
        }

        return clientProductValues;
    }

    private ClientProductValue toProductValue(Entity entity) {
        ClientProductValue clientProductValue = new ClientProductValue();
        clientProductValue.setCompanyProductId((Long) entity.getProperty("companyProductId"));
        clientProductValue.setValue(entity.getProperty("value")==null?"":entity.getProperty("value").toString());
        return clientProductValue;
    }


    private Entity getClientProductValueEntity(Key clientKey, ClientProductValue clientProductValue, DatastoreService datastore) throws EntityNotFoundException {
        Entity entity;
        Query.Filter filter =
                new Query.FilterPredicate("companyProductId",
                        Query.FilterOperator.EQUAL,
                        clientProductValue.getCompanyProductId());

        Query q = new Query("ClientProductValue").setAncestor(clientKey).setFilter(filter);
        PreparedQuery pq = datastore.prepare(q);
        entity = pq.asSingleEntity();
        if(entity == null){
            entity = new Entity("ClientProductValue", clientKey);
        }

        return entity;
    }

    private QueryResultList<Entity> getClientListQueryResult(ClientListQuery clientListQuery){
        int pageSize = 10;
        Key companyKey = KeyFactory.createKey("Company", clientListQuery.getCompanyId());
        FetchOptions fetchOptions = FetchOptions.Builder.withLimit(pageSize);

        if (clientListQuery.getPage() != null) {
            fetchOptions.startCursor(Cursor.fromWebSafeString(clientListQuery.getPage()));
        }

        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Query.Filter consultantFilter =
                new Query.FilterPredicate("consultantId",
                        Query.FilterOperator.EQUAL,
                        clientListQuery.getConsultantId());
        Query q = new Query("Client")
                .setAncestor(companyKey)
                .setFilter(consultantFilter)
                .addSort("clientName", Query.SortDirection.ASCENDING);
        /*if(clientListQuery.getSourceId() != null && clientListQuery.getStatusId()!=null){
            Query.Filter sourceIdFilter =
                    new Query.FilterPredicate("sourceId",
                            Query.FilterOperator.EQUAL,
                            clientListQuery.getSourceId());

            Query.Filter statusIdFilter =
                    new Query.FilterPredicate("statusId",
                            Query.FilterOperator.EQUAL,
                            clientListQuery.getStatusId());
            Query.Filter filter =
                    Query.CompositeFilterOperator.and(sourceIdFilter, statusIdFilter);

            q = q.setFilter(filter).addSort("clientName", Query.SortDirection.ASCENDING);
        }
        else if(clientListQuery.getSourceId() != null && clientListQuery.getStatusId()==null){
            Query.Filter filter =
                    new Query.FilterPredicate("sourceId",
                            Query.FilterOperator.EQUAL,
                            clientListQuery.getSourceId());
            q = q.setAncestor(companyKey).setFilter(filter).addSort("clientName", Query.SortDirection.ASCENDING);
        }
        else if(clientListQuery.getSourceId() == null && clientListQuery.getStatusId()!=null){
            Query.Filter filter =
                    new Query.FilterPredicate("statusId",
                            Query.FilterOperator.EQUAL,
                            clientListQuery.getStatusId());

            q = q.setAncestor(companyKey).setFilter(filter).addSort("clientName", Query.SortDirection.ASCENDING);
        }else if(clientListQuery.getCompanyName() != null){
            Query.Filter filter =
                    new Query.FilterPredicate("name",
                            Query.FilterOperator.IN,
                            clientListQuery.getCompanyName());

            q = q.setAncestor(companyKey).setFilter(filter).addSort("clientName", Query.SortDirection.ASCENDING);

        }
        else {
            q = q.setAncestor(companyKey).addSort("clientName", Query.SortDirection.ASCENDING);
        }*/
        PreparedQuery pq = datastore.prepare(q);
        return pq.asQueryResultList(fetchOptions);

    }
    private BaseClient toBaseClient(Entity entity)  {
        BaseClient client = new BaseClient();
            client.setCompanyId(entity.getParent().getId());
            client.setConsultantId(entity.getParent().getId());
            client.setClientId(entity.getKey().getId());
            client.setClientName((String)entity.getProperty("clientName"));
        return client;
    }
}
