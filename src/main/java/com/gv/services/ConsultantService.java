package com.gv.services;

import com.google.appengine.api.datastore.*;
import com.google.appengine.api.datastore.Query.CompositeFilterOperator;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.gv.beans.AuthResponse;
import com.gv.beans.Consultant;
import com.gv.beans.KeyBean;

import java.util.logging.Logger;

public class ConsultantService {
    private static final Logger log = Logger.getLogger(ConsultantService.class.getName());
	private static ConsultantService instance;
	private CompanyService companyService = CompanyService.getInstance();
	private ClientService clientService = ClientService.getInstance();
	private static CommonService commonService = new CommonService();

	public static ConsultantService getInstance() {
	  if(instance == null) {
		 instance = new ConsultantService();
	  }
	  return instance;
	}

	public KeyBean storeConsultant(Consultant consultant)  throws EntityNotFoundException {
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
		Entity entity = commonService.getDSEntity("Consultant", consultant.getConsultantId(), "Company", consultant.getCompanyId());
		entity.setProperty("name", consultant.getName());
		entity.setProperty("email", consultant.getEmail());
		entity.setProperty("password", consultant.getPassword());
		entity.setProperty("image", consultant.getImage());
		return new KeyBean(datastore.put(entity).getId());
	}

	public AuthResponse login(String email, String password){
		AuthResponse authResponse = new AuthResponse();
		DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();

		Filter emailFilter =
				new FilterPredicate("email",
						FilterOperator.EQUAL,
						email);

		Filter passwordFilter =
				new FilterPredicate("password",
						FilterOperator.EQUAL,
						password);

		Filter filter =
				CompositeFilterOperator.and(emailFilter, passwordFilter);

		Query q = new Query("Consultant").setFilter(filter);

		PreparedQuery pq = datastore.prepare(q);
		Entity entity = pq.asSingleEntity();
		if(entity == null){
			authResponse.setResult("INVALID_USER");
			authResponse.setUser(null);
		}else {
			authResponse.setResult("OK");
			Consultant consultant = toConsultant(entity);
			authResponse.setUser(consultant);
			authResponse.setConsultantClients(clientService.getConsultantClients(consultant.getCompanyId(), consultant.getConsultantId()));
		}
		return authResponse;
	}


	private Consultant toConsultant(Entity entity){
        Key companyKey = entity.getParent();
        Consultant consultant = new Consultant();
        consultant.setCompanyId(companyKey.getId());
        consultant.setConsultantId(entity.getKey().getId());
		consultant.setEmail((String) entity.getProperty("email"));
		consultant.setName((String)entity.getProperty("name"));
        consultant.setCompanySources( companyService.getCompanySources(companyKey));
        consultant.setCompanyStatus(companyService.getCompanyStatus(companyKey));
        //consultant.setCompanyProducts(clientService.getCompanyProducts(companyKey));
        //consultant.setClients(clientService.getConsultantClients(consultant.getCompanyId(), consultant.getConsultantId()));
        return consultant;
	}

}
