package com.gv.services;

import com.google.appengine.api.datastore.*;

/**
 * Created by mario on 8/7/16.
 */
public class CommonService {

    private static CommonService instance = null;
    private static DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
    protected CommonService() {
        // Exists only to defeat instantiation.
    }
    public static CommonService getInstance() {
        if(instance == null) {
            instance = new CommonService();
        }
        return instance;
    }

    public static Entity getDSEntity(String entityName, Long id) throws EntityNotFoundException {
        Entity entity;
        if(id != null) {
            Key key = KeyFactory.createKey(entityName, id);
            entity = datastore.get(key);
        }else {
            entity = new Entity(entityName);
        }
        return entity;
    }

    public Entity getDSEntity(String entityName, Long id, String parentName, Long parentId) throws EntityNotFoundException {
        Entity entity;
        Key parentKey = KeyFactory.createKey(parentName, parentId);
        if (id != null) {
            Key key = new KeyFactory.Builder(parentKey)
                .addChild(entityName, id).getKey();
            entity = datastore.get(key);
        } else {
            entity = new Entity(entityName, parentKey);
        }
        return entity;
    }

    public Entity getDSEntity(String entityName, Long id, String grandParentName, Long grandParentId, String parentName, Long parentId) throws EntityNotFoundException {
        Entity entity;
        Key grandParentKey = KeyFactory.createKey(grandParentName, grandParentId);

        if (id != null) {
            Key key = new KeyFactory.Builder(grandParentKey)
                .addChild(parentName, parentId)
                .addChild(entityName, id).getKey();
            entity = datastore.get(key);
        } else {
            Key key = new KeyFactory.Builder(grandParentKey)
                .addChild(parentName, parentId).getKey();
            entity = new Entity(entityName, key);
        }
        return entity;
    }
}
