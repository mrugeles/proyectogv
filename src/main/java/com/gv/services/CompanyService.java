package com.gv.services;

import com.google.appengine.api.datastore.*;
import com.gv.beans.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;


public class CompanyService {

    private static final Logger log = Logger.getLogger(ConsultantService.class.getName());
    private static CompanyService instance;
    private static CommonService commonService = new CommonService();

    public static CompanyService getInstance() {
        if(instance == null) {
            instance = new CompanyService();
        }
        return instance;
    }
    public KeyBean storeCompany(Company company) throws EntityNotFoundException {
        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Entity entity = commonService.getDSEntity("Company", company.getCompanyId());
        entity.setProperty("nit", company.getNit());
        entity.setProperty("name", company.getName());
        return new KeyBean(datastore.put(entity).getId());
    }
    public KeyBean storeCompanyProduct(CompanyProduct companyProduct) throws EntityNotFoundException {
        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Entity entity = commonService.getDSEntity("CompanyProduct", companyProduct.getCompanyProductId(), "Company", companyProduct.getCompanyId());
        entity.setProperty("companyId", companyProduct.getCompanyId());
        entity.setProperty("order", companyProduct.getOrder()   );
        entity.setProperty("name", companyProduct.getName());
        return new KeyBean(datastore.put(entity).getId());
    }

    public KeyBean storeCompanyStatus(CompanyStatus companyStatus) throws EntityNotFoundException {
        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Entity entity = commonService.getDSEntity("CompanyStatus", companyStatus.getId(), "Company", companyStatus.getCompanyId());
        entity.setProperty("companyId", companyStatus.getCompanyId());
        entity.setProperty("order", companyStatus.getOrder()   );
        entity.setProperty("name", companyStatus.getName());
        return new KeyBean(datastore.put(entity).getId());
    }

    public KeyBean storeCompanySource(CompanySource companySource) throws EntityNotFoundException {
        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Entity entity = commonService.getDSEntity("CompanySource", companySource.getId(), "Company", companySource.getCompanyId());
        entity.setProperty("companyId", companySource.getCompanyId());
        entity.setProperty("order", companySource.getOrder()   );
        entity.setProperty("name", companySource.getName());
        return new KeyBean(datastore.put(entity).getId());
    }

    public List<CompanySource> getCompanySources(long companyId){
        Key key = KeyFactory.createKey("Company", companyId);
        return getCompanySources(key);
    }

    public List<CompanySource> getCompanySources(Key companyKey){
        List<CompanySource> companySourcesList = new ArrayList<CompanySource>();
        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Query q = new Query("CompanySource")
                .setAncestor(companyKey)
                .addSort("name", Query.SortDirection.ASCENDING);
        PreparedQuery pq = datastore.prepare(q);
        Iterator<Entity> entityIterator = pq.asIterator();
        while(entityIterator.hasNext()){
            companySourcesList.add(this.toClientSource(entityIterator.next(), companyKey));
        }

        return companySourcesList;
    }

    public List<CompanyStatus> getCompanyStatus(long companyId){
        Key key = KeyFactory.createKey("Company", companyId);
        return getCompanyStatus(key);
    }
    public List<CompanyStatus> getCompanyStatus(Key companyKey){
        List<CompanyStatus> companyStatusList = new ArrayList<CompanyStatus>();
        DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
        Query q = new Query("CompanyStatus")
                .setAncestor(companyKey)
                .addSort("order", Query.SortDirection.ASCENDING);
        PreparedQuery pq = datastore.prepare(q);
        Iterator<Entity> entityIterator = pq.asIterator();
        while(entityIterator.hasNext()){
            companyStatusList.add(this.toCompanyStatus(entityIterator.next(), companyKey));
        }

        return companyStatusList;
    }

    public List<CompanyProduct> getCompanyProducts(Key companyKey){
        List<CompanyProduct> companyProductList = new ArrayList<CompanyProduct>();
        try {
            log.info(String.format("Company: %d", companyKey.getId()));
            DatastoreService datastore = DatastoreServiceFactory.getDatastoreService();
            Query q = new Query("CompanyProduct")
                    .setAncestor(companyKey)
                    .addSort("order", Query.SortDirection.ASCENDING);
            PreparedQuery pq = datastore.prepare(q);
            Iterator<Entity> entityIterator = pq.asIterator();
            while(entityIterator.hasNext()) {
                companyProductList.add(this.toCompanyProduct(entityIterator.next(), companyKey));
            }
        }catch (Exception ex){
            log.severe(ex.getMessage());
        }
        return companyProductList;
    }


    private CompanySource toClientSource(Entity entity, Key companyKey){
        CompanySource companySource = new CompanySource();
        companySource.setCompanyId(companyKey.getId());
        companySource.setId(entity.getKey().getId());
        companySource.setName(entity.getProperty("name").toString());
        return companySource;
    }

    private CompanyStatus toCompanyStatus(Entity entity, Key companyKey){
        CompanyStatus companyStatus = new CompanyStatus();
        companyStatus.setCompanyId(companyKey.getId());
        companyStatus.setId(entity.getKey().getId());
        companyStatus.setName(entity.getProperty("name").toString());
        return companyStatus;
    }

    private CompanyProduct toCompanyProduct(Entity entity, Key companyKey){
        CompanyProduct companyProduct = new CompanyProduct();
        companyProduct.setCompanyId(companyKey.getId());
        companyProduct.setCompanyProductId(entity.getKey().getId());
        companyProduct.setOrder(new Integer(entity.getProperty("order").toString()));
        companyProduct.setName(entity.getProperty("name").toString());
        return companyProduct;
    }
}
