package com.gv.beans;

import com.google.appengine.api.datastore.Blob;

import java.util.List;

public class Consultant {
    private Long companyId;
    private Long consultantId;
    private String name;
    private String email;
    private String password;
    private Blob image;
    private List<CompanySource> companySources;
    private List<CompanyStatus> companyStatus;
    private List<CompanyProduct> companyProducts;
    private List<BaseClient> clients;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Blob getImage() {
        return image;
    }

    public void setImage(Blob image) {
        this.image = image;
    }

    public Long getConsultantId() {
        return consultantId;
    }

    public void setConsultantId(Long consultantId) {
        this.consultantId = consultantId;
    }

    public List<CompanySource> getCompanySources() {
        return companySources;
    }

    public void setCompanySources(List<CompanySource> companySources) {
        this.companySources = companySources;
    }

    public List<CompanyStatus> getCompanyStatus() {
        return companyStatus;
    }

    public void setCompanyStatus(List<CompanyStatus> companyStatus) {
        this.companyStatus = companyStatus;
    }

    public List<BaseClient> getClients() {
        return clients;
    }

    public void setClients(List<BaseClient> clients) {
        this.clients = clients;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }


    public List<CompanyProduct> getCompanyProducts() {
        return companyProducts;
    }

    public void setCompanyProducts(List<CompanyProduct> companyProducts) {
        this.companyProducts = companyProducts;
    }
}
