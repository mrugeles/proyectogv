package com.gv.beans;

import com.google.appengine.api.datastore.Text;

import java.util.List;

public class Client extends BaseClient{
	private Long sourceId;
	private Long statusId;
	private boolean enabled;
	private String contactName;
	private String documentType;
	private String documentNumber;
	private String phoneNumber;
	private String phoneNumber2;
	private String address;
	private String email;
	private ClientLog clientLog;
	private List<ClientLog> clientLogs;
	private List<ClientProductValue> productsValues;


	public String getContactName() {
		return contactName;
	}
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	public String getPhoneNumber2() {
		return phoneNumber2;
	}
	public void setPhoneNumber2(String phoneNumber2) {
		this.phoneNumber2 = phoneNumber2;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}


	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getDocumentNumber() {
		return documentNumber;
	}

	public void setDocumentNumber(String documentNumber) {
		this.documentNumber = documentNumber;
	}

	public Long getSourceId() {
		return sourceId;
	}

	public void setSourceId(Long sourceId) {
		this.sourceId = sourceId;
	}

	public Long getStatusId() {
		return statusId;
	}

	public void setStatusId(Long statusId) {
		this.statusId = statusId;
	}

	public List<ClientLog> getClientLogs() {
		return clientLogs;
	}

	public void setClientLogs(List<ClientLog> clientLogs) {
		this.clientLogs = clientLogs;
	}

	public ClientLog getClientLog() {
		return clientLog;
	}

	public void setClientLog(ClientLog clientLog) {
		this.clientLog = clientLog;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public List<ClientProductValue> getProductsValues() {
		return productsValues;
	}

	public void setProductsValues(List<ClientProductValue> productsValues) {
		this.productsValues = productsValues;
	}

	@Override
	public boolean equals(Object o) {

		if (o == this) return true;
		if (!(o instanceof Client)) {
			return false;
		}

		Client client = (Client) o;

		return client.getClientId().equals(getClientId()) &&
			client.getCompanyId().equals(getCompanyId()) &&
			client.getConsultantId().equals(getConsultantId()) &&
			client.getStatusId().equals(getStatusId()) &&
			client.getSourceId().equals(getSourceId()) &&
			client.isEnabled() == isEnabled() &&
			client.getClientName().equals(getClientName()) &&
			client.getContactName().equals(getContactName()) &&
			client.getDocumentType().equals(getDocumentType()) &&
			client.getDocumentNumber().equals(getDocumentNumber()) &&
			client.getAddress().equals(getAddress()) &&
			client.getPhoneNumber().equals(getPhoneNumber()) &&
			client.getPhoneNumber2().equals(getPhoneNumber2()) &&
			client.getEmail().equals(getEmail());
	}
}
