package com.gv.beans;

import java.util.Date;

/**
 * Created by mario on 7/31/16.
 */
public class Task {

    public static Integer RED;
    public static Integer YELLOW;
    public static Integer GREEN;

    private Long taskId;
    private Long companyId;
    private Long clientId;

    public Long getConsultantId() {
        return consultantId;
    }

    public void setConsultantId(Long consultantId) {
        this.consultantId = consultantId;
    }

    private Long consultantId;
    private Long taskTypeId;
    private Date taskDate;
    private String clientName;
    private String taskTypeName;
    private String description;
    private String response;
    private Integer status;
    private Integer order;

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public Long getTaskTypeId() {
        return taskTypeId;
    }

    public void setTaskTypeId(Long taskTypeId) {
        this.taskTypeId = taskTypeId;
    }

    public Date getTaskDate() {
        return taskDate;
    }

    public void setTaskDate(Date taskDate) {
        this.taskDate = taskDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    @Override
    public boolean equals(Object o) {

        if (o == this) return true;
        if (!(o instanceof Task)) {
            return false;
        }

        Task task = (Task) o;

        return task.getTaskId().equals(taskId) &&
               task.getClientId().equals(clientId) &&
               task.getConsultantId().equals(consultantId) &&
               task.getTaskTypeId().equals(taskTypeId) &&
               task.getTaskDate().compareTo(taskDate) == 0 &&
               task.getDescription().equals(description) &&
               task.getResponse().equals(response) &&
               task.getOrder().equals(order) &&
               task.getStatus().equals(status);
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getTaskTypeName() {
        return taskTypeName;
    }

    public void setTaskTypeName(String taskTypeName) {
        this.taskTypeName = taskTypeName;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }
}
