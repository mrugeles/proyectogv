package com.gv.beans;

public class KeyBean {
	private Long id;

	public KeyBean(Long id) {
		super();
		this.id = id;
	}

	public Long getKeyId() {
		return id;
	}

	public void setKeyId(Long key) {
		this.id = id;
	}
}
