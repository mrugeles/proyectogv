package com.gv.beans;

import java.util.List;

/**
 * Created by mario on 11/14/15.
 */
public class AuthResponse {
    private String result;
    private Consultant user;
    private List<BaseClient> consultantClients;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Consultant getUser() {
        return user;
    }

    public void setUser(Consultant user) {
        this.user = user;
    }

    public List<BaseClient> getConsultantClients() {
        return consultantClients;
    }

    public void setConsultantClients(List<BaseClient> consultantClients) {
        this.consultantClients = consultantClients;
    }
}
