package com.gv.beans;

public class CompanyProduct {
    private Long companyId;
    private Long companyProductId;
    private int order;
    private String name;


    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }

    public Long getCompanyProductId() {
        return companyProductId;
    }

    public void setCompanyProductId(Long companyProductId) {
        this.companyProductId = companyProductId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }
}
