package com.gv.beans;

import java.util.List;

/**
 * Created by mario on 11/1/15.
 */
public class ClientsList {
    private String page;
    private List<Client> clientsList;

    public ClientsList(String page, List<Client> clientsList) {
        this.page = page;
        this.clientsList = clientsList;
    }

    public List<Client> getClientsList() {
        return clientsList;
    }

    public void setClientsList(List<Client> clientsList) {
        this.clientsList = clientsList;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }
}
