package com.gv.beans;

public class CompanyStatus extends GenericEntity{

    private int order;

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }
}
