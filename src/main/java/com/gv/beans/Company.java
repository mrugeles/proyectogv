package com.gv.beans;

/**
 * Created by mario on 10/25/15.
 */
public class Company {
    private Long companyId;
    private String nit;
    private String name;
    private KeyBean keyBean;

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public KeyBean getKeyBean() {
        return keyBean;
    }

    public void setKeyBean(KeyBean keyBean) {
        this.keyBean = keyBean;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }
}
