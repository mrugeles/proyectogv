package com.gv.beans;

import com.google.appengine.api.datastore.Text;

import java.util.Date;

/**
 * Created by mario on 11/17/15.
 */
public class ClientLog {
    private Date logDate;
    private Text log;

    public Date getLogDate() {
        return logDate;
    }

    public void setLogDate(Date logDate) {
        this.logDate = logDate;
    }

    public Text getLog() {
        return log;
    }

    public void setLog(Text log) {
        this.log = log;
    }
}
