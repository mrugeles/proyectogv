package com.gv.beans;

/**
 * Created by mario on 12/8/15.
 */
public class ClientProductValue {
    private Long companyId;
    private Long clientId;
    private Long companyProductId;
    private String value;

    public Long getCompanyProductId() {
        return companyProductId;
    }

    public void setCompanyProductId(Long companyProductId) {
        this.companyProductId = companyProductId;
    }

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }
}
