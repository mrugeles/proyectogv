package com.gv.endpoints;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiMethod.HttpMethod;
import com.google.api.server.spi.config.Named;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.gv.beans.*;
import com.gv.services.ClientService;
import com.gv.services.CompanyService;
import com.gv.services.ConsultantService;
import com.gv.services.TaskService;

import java.util.List;

@Api(name="consultantAPI",
     version="v1")
public class ConsultantAPI {
    private static ClientService clientService = new ClientService();
    private static TaskService taskService = new TaskService();
	private CompanyService companyService = CompanyService.getInstance();

	@ApiMethod(name="storeClient",
			   path="storeClient",
			   httpMethod=HttpMethod.POST)
	public KeyBean storeClient(Client client){
		KeyBean clientId = null;
		try {
			clientId = clientService.storeClient(client);
		} catch (EntityNotFoundException e) {
			e.printStackTrace();
		}
		return clientId;
	}

	@ApiMethod(name="storeClientProducts",
			path="storeClientProducts",
			httpMethod=HttpMethod.POST)
	public KeyBean storeClientProducts(Client client){
		KeyBean clientId = null;
		try {
			clientId = clientService.storeClientProducts(client);
		} catch (EntityNotFoundException e) {
			e.printStackTrace();
		}
		return clientId;
	}
	
	@ApiMethod(name="getClient",
			   path="getClient",
			   httpMethod=HttpMethod.POST)
	public Client getClient(Client c){
		Client client = null;
		try {
			client = clientService.getClient(c);
		} catch (EntityNotFoundException e) {
			e.printStackTrace();
		}
		return client;
	}
	
	@ApiMethod(name="getClientsList",
			path="getClientsList",
			httpMethod=HttpMethod.POST)
	public ClientsList getClientsList(ClientListQuery clientListQuery){
		return clientService.getClientsList(clientListQuery);
	}

	@ApiMethod(name="getCompanyStatus",
			path="getCompanyStatus",
			httpMethod=HttpMethod.POST)
	public List<CompanyStatus> getCompanyStatus(Company company){
		return companyService.getCompanyStatus(company.getCompanyId());
	}

	@ApiMethod(name="getCompanySources",
			path="getCompanySources",
			httpMethod=HttpMethod.POST)
	public List<CompanySource> getCompanySources(Company company){
		return companyService.getCompanySources(company.getCompanyId());
	}

	@ApiMethod(name="getConsultantTasks",
		path="getConsultantTasks",
		httpMethod=HttpMethod.POST)
	public List<Task> getConsultantTasks(Task task){
		return taskService.getConsultantTasks(task);
	}

	@ApiMethod(name="getCompanyTaskTypes",
			path="getCompanyTaskTypes",
			httpMethod=HttpMethod.POST)
	public List<TaskType> getCompanyTaskTypes(Consultant consultant){
		return taskService.getCompanyTaskTypes(consultant.getCompanyId());
	}

    @ApiMethod(name="getConsultantClients",
            path="getConsultantClients",
            httpMethod=HttpMethod.POST)
    public List<BaseClient> getConsultantClients(Consultant consultant){
        return clientService.getConsultantClients(consultant.getCompanyId(), consultant.getConsultantId());
    }

	@ApiMethod(name="getClientTasks",
			path="getClientTasks",
			httpMethod=HttpMethod.POST)
	public List<Task> getClientTasks(Task task){
		return taskService.getClientTasks(task);
	}

	@ApiMethod(name="storeTask",
			path="storeTask",
			httpMethod=HttpMethod.POST)
	public KeyBean storeTask(Task task){
		KeyBean key = null;
		try {
			key = taskService.storeTask(task);
		} catch (EntityNotFoundException e) {
			e.printStackTrace();
		}
		return key;
	}

	@ApiMethod(name="login",
			path="login",
			httpMethod= ApiMethod.HttpMethod.POST)
	public AuthResponse login(@Named("login") String login, @Named("password") String password){
		return ConsultantService.getInstance().login(login, password);
	}


}
