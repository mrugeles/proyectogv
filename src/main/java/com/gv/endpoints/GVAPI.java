package com.gv.endpoints;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Text;
import com.gv.beans.*;
import com.gv.services.*;

import java.util.*;

@Api(name="gvAPI",
        version="v1")
public class GVAPI {
    private static ClientService clientService = new ClientService();
    private static CompanyService companyService = new CompanyService();
    private static ConsultantService consultantService = new ConsultantService();
    private static TaskService taskService = new TaskService();


    /*Agregar GVUser para verificar autenticación*/
    @ApiMethod(name="storeCompany",
            path="storeCompany",
            httpMethod= ApiMethod.HttpMethod.POST)
    public KeyBean storeCompany(Company company){
        KeyBean clientId = null;
        try {
            clientId = companyService.storeCompany(company);
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
        }
        return clientId;
    }

    @ApiMethod(name="storeCompanyProduct",
            path="storeCompanyProduct",
            httpMethod= ApiMethod.HttpMethod.POST)
    public KeyBean storeCompanyProduct(CompanyProduct companyProduct){
        KeyBean clientId = null;
        try {
            clientId = companyService.storeCompanyProduct(companyProduct);
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
        }
        return clientId;
    }

    /*Agregar GVUser para verificar autenticación*/
    @ApiMethod(name="storeConsultant",
            path="storeConsultant",
            httpMethod= ApiMethod.HttpMethod.POST)
    public KeyBean storeConsultant(Consultant consultant){
        KeyBean id = null;
        try {
            id = consultantService.storeConsultant(consultant);
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
        }
        return id;
    }


    @ApiMethod(name="storeCompanySource",
        path="storeCompanySource",
        httpMethod= ApiMethod.HttpMethod.POST)
    public void storeClientSource(CompanySource companySource){
        try {
            companyService.storeCompanySource(companySource);
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
        }
    }

    @ApiMethod(name="storeCompanyStatus",
        path="storeCompanyStatus",
        httpMethod= ApiMethod.HttpMethod.POST)
    public void storeClientStatus(CompanyStatus companyStatus){
        try {
            companyService.storeCompanyStatus(companyStatus);
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
        }
    }
    /*Agregar GVUser para verificar autenticación*/
    @ApiMethod(name="createTestData",
        path="createTestData",
        httpMethod= ApiMethod.HttpMethod.POST)
    public KeyBean createTestData(){
        KeyBean id = null;
        try {
            this.fakeData();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return id;
    }

    public KeyBean fakeData(){
        Company company = new Company();
        company.setName("Company 1");
        company.setNit("0001");
        KeyBean companyKey = null;
        try {
            companyKey = companyService.storeCompany(company);
            String[] products = new String[]{"VOZ", "APP", "BA", "INGRESO"};
            int i = 1;
            for (String product: products) {
                CompanyProduct companyProduct = new CompanyProduct();
                companyProduct.setName(product);
                companyProduct.setCompanyId(companyKey.getKeyId());
                companyProduct.setOrder(i++);
                companyService.storeCompanyProduct(companyProduct);
            }
            String[] statusList = new String[]{"CLIENTE CONTACTADO", "OFERTA PRESENTADA", "CLIENTE ACEPTA LA OFERTA", "CONTRATO FIRMADO", "EN CRÈDITO/INGRESOS", "EN ACTIVACIONES/ CONTROL I", "ALTA/INSTALADA", "CLIENTE DECLINA"};
            i = 1;
            List<CompanyStatus> companyStatusList = new ArrayList<>();
            for (String status: statusList) {
                CompanyStatus companyStatus = new CompanyStatus();
                companyStatus.setName(status);
                companyStatus.setCompanyId(companyKey.getKeyId());
                companyStatus.setOrder(i++);
                companyStatus.setId(companyService.storeCompanyStatus(companyStatus).getKeyId());
                companyStatusList.add(companyStatus);
            }


            String[] sources = new String[]{"TELEMERCADEO", "BARRIDO", "BASE ASIGNADA", "REFERIDO EXTERNO", "REFERIDO INTERNO"};
            i = 1;
            for (String source: sources) {
                CompanySource companySource = new CompanySource();
                companySource.setName(source);
                companySource.setCompanyId(companyKey.getKeyId());
                companySource.setOrder(i++);
                companyService.storeCompanySource(companySource);
            }

            List<TaskType> taskTypeList= new ArrayList<>();
            String[] taskTypes = new String[]{"CITA", "SERVICIO", "TAREA"};
            for (String source: taskTypes) {
                TaskType taskType = new TaskType();
                taskType.setName(source);
                taskType.setCompanyId(companyKey.getKeyId());
                KeyBean taskTypeBean = taskService.storeTaskType(taskType);
                taskType.setId(taskTypeBean.getKeyId());
                taskTypeList.add(taskType);
            }

            Consultant consultant = new Consultant();
            consultant.setCompanyId(companyKey.getKeyId());
            consultant.setName("John Doe");
            consultant.setEmail("consultor@gmail.com");
            consultant.setPassword("consultor.2016");
            KeyBean keyBeanConsultant = consultantService.storeConsultant(consultant);

            for(i = 15; i < 31; i++) {
                for (int clientNumber = 0; clientNumber < 5; clientNumber++) {
                    Client client = new Client();
                    client.setCompanyId(companyKey.getKeyId());
                    client.setConsultantId(keyBeanConsultant.getKeyId());
                    client.setEnabled(true);
                    client.setClientName("Client Name " + i+"-"+clientNumber);
                    client.setContactName("Contact Name " + clientNumber);
                    client.setDocumentType("CC");
                    client.setDocumentNumber("DocumentNumber" + clientNumber);
                    client.setAddress("Address " + clientNumber);
                    client.setPhoneNumber("Phone Number " + clientNumber);
                    client.setPhoneNumber2("Phone Number2- " + clientNumber);
                    client.setStatusId(companyStatusList.get(0).getId());

                    ClientLog clientLog = new ClientLog();
                    clientLog.setLog(new Text("test"));
                    clientLog.setLogDate(new Date());
                    client.setClientLog(clientLog);
                    KeyBean keyBean = ClientService.getInstance().storeClient(client);
                    Random r = new Random();
                    for(int taskN = 0; taskN < 5; taskN++) {
                        TaskType taskType = taskTypeList.get(r.nextInt(3));
                        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("America/Bogota"));
                        Task task = new Task();
                        task.setCompanyId(companyKey.getKeyId());
                        task.setClientId(keyBean.getKeyId());
                        task.setConsultantId(keyBeanConsultant.getKeyId());
                        task.setDescription("Description " + taskType.getName());
                        task.setResponse("Response " + taskType.getName());
                        task.setOrder(r.nextInt(10));
                        task.setStatus(getRandom(1, 3, r));
                        calendar.set(Calendar.YEAR, 2016);
                        calendar.set(Calendar.MONTH, calendar.OCTOBER);
                        calendar.set(Calendar.DAY_OF_MONTH, (i));

                        if (r.nextInt(10) % 2 == 0) {
                            calendar.set(Calendar.HOUR_OF_DAY, 0);
                            calendar.set(Calendar.MINUTE, 0);
                            calendar.set(Calendar.SECOND, 0);
                        }
                        task.setTaskDate(calendar.getTime());
                        task.setTaskTypeId(taskType.getId());
                        taskService.storeTask(task);
                    }
                }
            }
        } catch (EntityNotFoundException e) {
            e.printStackTrace();
        }
        return companyKey;
    }

    private int getRandom(int aStart, int aEnd, Random aRandom){
        if (aStart > aEnd) {
            throw new IllegalArgumentException("Start cannot exceed End.");
        }
        //get the range, casting to long to avoid overflow problems
        long range = (long)aEnd - (long)aStart + 1;
        // compute a fraction of the range, 0 <= frac < range
        long fraction = (long)(range * aRandom.nextDouble());
        return (int)(fraction + aStart);
    }
}
